<?php 
session_start();
if(empty($_SESSION['is_loggedin'])){
    header('Location:/login.php');
}

require_once("../connexion.php");

/* requête pour récupérer les 25 premiers enregistrements de la table client trié par nom */
$sql = "SELECT * FROM demo_fleuriste.`client` ORDER BY nom, prenom LIMIT 25;";
$query = $db->prepare($sql);
$query->execute([]);

$clients = $query->fetchAll();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fleuriste</title>
    <link rel="stylesheet" href="../assets/style.css">
</head>
<body>
    <header>
        <nav>
            <ul>
                <li>
                    <a href="/client/index.php">Gestion des clients</a>
                </li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </nav>
    </header>
    <main>
        <h1>Gestion Client</h1>
        <a href="./addClient.php">Ajouter un Client</a>
        <table>
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Adresse</th>
                    <th>Contact</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($clients as $client){ ?>
                    <tr>
                        <td>
                            <a href="viewClient.php?id=<?= $client['id_client'] ?>"><?= $client['prenom'] .' '. $client['nom']; ?></a>
                        </td>
                        <td>
                            <?= $client['adresse']; ?><br>
                            <?= $client['code_postal'] . ' ' . $client['ville']; ?>
                        </td>
                        <td>
                            <?= $client['telephone']; ?>
                        </td>
                        <td>
                            <a href="editClient.php?id=<?= $client['id_client'] ?>">Modifier</a><br>
                            <a href="deleteClient.php?id=<?= $client['id_client'] ?>">Supprimer</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </main>
</body>
</html>
