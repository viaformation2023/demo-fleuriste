<?php 
session_start();
if(empty($_SESSION['is_loggedin'])){
    header('Location:/login.php');
}

require_once("../connexion.php");

$erreur = "";
if (isset($_POST['submit'])){

    if(isset($_POST['lname']) && $_POST['lname'] != ''
    && isset($_POST['fname']) && $_POST['fname'] != ''
    && isset($_POST['tel']) && $_POST['tel'] != '' ) {

        $nom = htmlspecialchars(trim($_POST['lname']));
        $prenom = htmlspecialchars(trim($_POST['fname']));
        $tel = htmlspecialchars(trim($_POST['tel']));
        $address = isset($_POST['address']) ? htmlspecialchars(trim($_POST['address'])) : 'NULL';
        $cp = isset($_POST['cp']) ? htmlspecialchars(trim($_POST['cp'])) : 'NULL';
        $city = isset($_POST['city']) ? htmlspecialchars(trim($_POST['city'])) : null;

        $sql = "INSERT INTO demo_fleuriste.client(nom, prenom, telephone, adresse, code_postal, ville)
        VALUES (:nom, :prenom, :tel, :adresse, :cp, :ville);";
        $query = $db->prepare($sql);
        $query->execute([
            'nom' => $nom, 
            'prenom' => $prenom, 
            'tel' => $tel, 
            'adresse' => $address, 
            'cp' => $cp, 
            'ville' => $city
        ]);

        header('Location:index.php');
    } 
    else {
        $erreur = "<p class='error'>Vous m'avez pas renseigné tous les champs obligatoires.</p>";
    }
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fleuriste</title>
    <link rel="stylesheet" href="../assets/style.css">
</head>
<body>
    <header>
        <nav>
            <ul>
                <li>
                    <a href="/client/index.php">Gestion des clients</a>
                </li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </nav>
    </header>
    <main>
        <h1>Ajouter un Client</h1>
        <form action="" method="post">
            <?= $erreur; ?>
            <div>
                <input type="text" name="lname" id="lname" placeholder="Nom" required>
                <input type="text" name="fname" id="fname" placeholder="Prénom" required>
            </div>
            <div>
                <input type="tel" name="tel" id="tel" placeholder="Numéro de téléphone" required>
            </div>
            <div>
                <input type="text" name="address" id="address" placeholder="Numéro et nom de rue">
            </div>
            <div>
                <input type="text" name="cp" id="cp" placeholder="Code Postal">
                <input type="text" name="city" id="city" placeholder="Ville">
            </div>
            <div>
                <input type="submit" name="submit" value="Enregister">
            </div>
        </form> 
    </main>
</body>
</html>
