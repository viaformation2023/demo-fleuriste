<?php 
session_start();
if(empty($_SESSION['is_loggedin'])){
    header('Location:/login.php');
}

require_once("../connexion.php");

/* je renvois l'utilisateur à la page index s'il n'y a pas de parametre id dans l'url de la page */
if (!isset($_GET['id']) || intval($_GET['id']) == 0){
    header('Location:index.php');
}


$id = $_GET['id'];
/* requete pour récupérer les informations d'un client */
$sql = "SELECT * FROM demo_fleuriste.`client` WHERE id_client = :id;";
$query = $db->prepare($sql);
$query->execute([
    'id' => $id
]);

$client = $query->fetch();

/* je renvois l'utilisateur à la page index si le client n'existe pas en base */
if ($client === false){
    header('Location:index.php');
}


$erreur = "";
if (isset($_POST['submit'])){

    if(isset($_POST['lname']) && $_POST['lname'] != ''
    && isset($_POST['fname']) && $_POST['fname'] != ''
    && isset($_POST['tel']) && $_POST['tel'] != ''
    && isset($_POST['id']) && intval($_POST['id']) != 0) {

        $id = intval(trim($_POST['id']));
        $nom = htmlspecialchars(trim($_POST['lname']));
        $prenom = htmlspecialchars(trim($_POST['fname']));
        $tel = htmlspecialchars(trim($_POST['tel']));
        $address = isset($_POST['address']) ? htmlspecialchars(trim($_POST['address'])) : 'NULL';
        $cp = isset($_POST['cp']) ? htmlspecialchars(trim($_POST['cp'])) : 'NULL';
        $city = isset($_POST['city']) ? htmlspecialchars(trim($_POST['city'])) : null;

        $sqlUpdate = "UPDATE demo_fleuriste.client 
                SET nom = :nom, prenom = :prenom, telephone = :telephone, adresse = :adresse, code_postal = :cp, ville = :ville
                WHERE id_client = :id";
        $queryUpdate = $db->prepare($sqlUpdate);
        $queryUpdate->execute([
            "nom" => $nom, 
            "prenom" => $prenom, 
            "telephone" => $tel, 
            "adresse" => $address,
            "cp" => $cp, 
            "ville" => $city,
            "id" => $id
        ]);
        header('Location:viewClient.php?id='.$id);
    } 
    else {
        $erreur = "<p class='error'>Vous m'avez pas renseigné tous les champs obligatoires.</p>";
    }
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fleuriste</title>
    <link rel="stylesheet" href="../assets/style.css">
</head>
<body>
    <header>
        <nav>
            <ul>
                <li>
                    <a href="/client/index.php">Gestion des clients</a>
                </li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </nav>
    </header>
    <main>
        <h1>Modifier les informations de <?= $client['prenom'] ." ". $client['nom']; ?></h1>
        <form action="" method="post">
            <?= $erreur; ?>
            <input type="hidden" name="id" value="<?= $id; ?>">
            <div>
                <input type="text" name="lname" id="lname" placeholder="Nom" value="<?= $client['nom']; ?>" required>
                <input type="text" name="fname" id="fname" placeholder="Prénom" value="<?= $client['prenom']; ?>" required>
            </div>
            <div>
                <input type="tel" name="tel" id="tel" placeholder="Numéro de téléphone" value="<?= $client['telephone']; ?>"  required>
            </div>
            <div>
                <input type="text" name="address" id="address" placeholder="Numéro et nom de rue" value="<?= $client['adresse']; ?>" >
            </div>
            <div>
                <input type="text" name="cp" id="cp" placeholder="Code Postal" value="<?= $client['code_postal']; ?>" >
                <input type="text" name="city" id="city" placeholder="Ville" value="<?= $client['ville']; ?>" >
            </div>
            <div>
                <input type="submit" name="submit" value="Modifier">
            </div>
        </form> 
    </main>
</body>
</html>
