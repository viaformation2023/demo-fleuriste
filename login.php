<?php 
session_start();
require_once("connexion.php");

$erreur = "";

 /* je vérifie que l'utilisateur a bien saisie les informations obligatoires */
if(isset($_POST['submit']) && !empty($_POST['username']) && !empty($_POST['password'])){
    $username = trim($_POST['username']);

    $sql = "SELECT `username`, `password` FROM demo_fleuriste.user WHERE username = :user";
    $query = $db->prepare($sql);
    $query->execute([
        "user" => $username
    ]);

    $user = $query->fetch();

    if ($user && password_verify($_POST['password'], $user['password'])){
        $pwd = $_POST['password'];   //ce qui est saisi dans le formulaire
        $hashedpwd = $user['password'];    //le hash sauvegardé dans la table user
        
        $_SESSION['is_loggedin'] = true;
        header('Location:index.php');
    } else {
        $erreur = "<p>Informations erronnées</p>";
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fleuriste</title>
    <link rel="stylesheet" href="assets/style.css">
</head>
<body>
    <?= $erreur; ?>
    <form method="post">
        <input type="text" name="username" id="username" placeholder="Nom d'utilisateur">
        <input type="password" name="password" id="password" placeholder="Mot de passe">
        <input type="submit" name="submit" value="Se connecter">
    </form>
</body>
</html>