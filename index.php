<?php
session_start();
if(empty($_SESSION['is_loggedin'])){
    header('Location:/login.php');
}

require_once("./connexion.php");

$sql = "SELECT fleur.id_fleur, variete.libelle AS variete, couleur.libelle AS couleur, COUNT(ligne_commande.num_commande) AS nbVentes 
FROM demo_fleuriste.`ligne_commande`
INNER JOIN demo_fleuriste.fleur ON ligne_commande.id_fleur = fleur.id_fleur
INNER JOIN demo_fleuriste.variete ON fleur.id_variete = variete.id_variete
INNER JOIN demo_fleuriste.couleur ON fleur.id_couleur = couleur.id_couleur
INNER JOIN demo_fleuriste.fournisseur_fleur ON fleur.id_fleur = fournisseur_fleur.id_fleur
GROUP BY fleur.id_fleur
ORDER BY nbVentes DESC
LIMIT 10;";
$query = $db->prepare($sql);
$query->execute();

$topFleurs = $query->fetchAll();

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fleuriste</title>
    <link rel="stylesheet" href="assets/style.css">
</head>
<body>
    <header>
        <nav>
            <ul>
                <li>
                    <a href="/client/index.php">Gestion des clients</a>
                </li>
                <li><a href="/user/index.php">Gestion des utilisateurs</a></li>
                <li></li>
                <li></li>
                <li><a href="/logout.php">Déconnexion</li>
            </ul>
        </nav>
    </header>
    <main>
        <h1>Top 10 des ventes de fleurs</h1>
        <table>
            <thead>
                <th>Variété</th>
                <th>Couleur</th>
                <th>Nb Ventes</th>
                <th>Stock</th>
            </thead>
            <tbody>
                <?php foreach($topFleurs as $fleur){ 
                    $sqlStock = "SELECT SUM(stock) AS stock FROM demo_fleuriste.fleur
                    INNER JOIN demo_fleuriste.fournisseur_fleur ON fournisseur_fleur.id_fleur = fleur.id_fleur
                    WHERE fleur.id_fleur = :id
                    GROUP BY fleur.id_fleur; ";
                    
                    $queryStock = $db->prepare($sqlStock);
                    $queryStock->execute([
                        'id' => $fleur['id_fleur']
                    ]);
                    $stock = $queryStock->fetch();
                    ?>
                    <tr>
                        <td><?= $fleur['variete']; ?></td>
                        <td><?= $fleur['couleur']; ?></td>
                        <td><?= $fleur['nbVentes']; ?></td>
                        <td><?= $stock['stock']; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </main>
</body>
</html>