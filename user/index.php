<?php 
session_start();
if(empty($_SESSION['is_loggedin'])){
    header('Location:/login.php');
}

require_once("../connexion.php");

/* requête pour récupérer les 25 premiers enregistrements de la table user */
$sql = "SELECT * FROM demo_fleuriste.`user` LIMIT 25;";
$query = $db->prepare($sql);
$query->execute([]);

$users = $query->fetchAll();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fleuriste</title>
    <link rel="stylesheet" href="../assets/style.css">
</head>
<body>
    <header>
        <nav>
            <ul>
                <li>
                    <a href="/client/index.php">Gestion des clients</a>
                </li>
                <li><a href="/user/index.php">Gestion des utilisateurs</a></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </nav>
    </header>
    <main>
        <h1>Gestion des Utilisateurs</h1>
        <a href="./addUser.php">Ajouter un Utilisateur</a>
        <table>
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($users as $user){ ?>
                    <tr>
                        <td>
                            <?= $user['username'] ?>
                        </td>
                        <td>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </main>
</body>
</html>
