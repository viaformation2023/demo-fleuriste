<?php 
session_start();
if(empty($_SESSION['is_loggedin'])){
    header('Location:/login.php');
}

require_once("../connexion.php");

if(isset($_POST['submit'])  && !empty($_POST['username']) && !empty($_POST['password'])){
    $username = trim($_POST['username']);

//il faudrait faire un select dans la table user pour vérifier que username n'est pas déjà pris


    $hashedpwd = password_hash($_POST['password'], PASSWORD_DEFAULT);

    $sql = "INSERT INTO demo_fleuriste.`user`(`username`, `password`) VALUE (:user, :pwd)";
    $query = $db->prepare($sql);
    $query->execute([
        "user" => $username,
        "pwd"  => $hashedpwd
    ]);

    header('Location:index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajouter un utilisateur</title>
    <link rel="stylesheet" href="../assets/style.css">
</head>
<body>
    <form method="post">
        <div>
        <input type="text" name="username" id="username" placeholder="Nom d'utilisateur">
        </div>
        <div>
        <input type="password" name="password" id="password" placeholder="Mot de passe">
        </div>
        <input type="submit" name="submit" value="Ajouter">
    </form>
</body>
</html>